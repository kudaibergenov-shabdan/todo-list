import {useState} from "react";
import './App.css';
import Task from "./components/Task/Task";
import AddTaskForm from "./components/Task/AddTaskForm";

const App = () => {
    const [tasks, setTasks] = useState([
        {taskName: "Chill out", id: "1", done: false},
        {taskName: "Dream about", id: "2", done: false},
        {taskName: "Play life", id: "3", done: false},
    ]);

    const [currentTask, setCurrentTask] = useState({taskName: ""});

    const inputTaskChange = (inputCurrentTask) => {
        setCurrentTask({taskName: inputCurrentTask});
    };

    const addTask = (e, newTask) => {
        e.preventDefault();
        if (newTask) {
            setTasks([...tasks, {taskName: newTask, id: tasks.length + 1, done: false}]);
        }
    }

    const deleteTask = id => {
        setTasks(
            tasks.filter(task => task.id !== id)
        );
    };

    const taskDone = id => {
        setTasks(
            tasks.map(task => {
                if (task.id === id) {
                    return {...task, done: !(task.done)};
                }
                return task;
            })
        )
    };

    const tasksComponent = tasks.map(task => (
        <Task
            key={task.id}
            task={task.taskName}
            done={task.done}
            onRemove={() => deleteTask(task.id)}
            onCheck={() => taskDone(task.id)}
        />
    ));

    return (
        <div className="App">
            <AddTaskForm
                curTask={currentTask.taskName}
                onAdd={(e) => addTask(e, currentTask.taskName)}
                onInputChange={(e) => inputTaskChange(e.target.value)}/>
            {tasksComponent}
        </div>
    );
}

export default App;
