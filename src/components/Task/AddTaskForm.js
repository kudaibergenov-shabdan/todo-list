import './AddTaskForm.css';

const AddTaskForm = props => {
    return (
        <form className="task-form" onSubmit={props.onAdd}>
            <input
                type="text"
                placeholder="Add task"
                onChange={props.onInputChange}
            />
            <button type="submit">Add task</button>
        </form>
    );
};

export default AddTaskForm;
