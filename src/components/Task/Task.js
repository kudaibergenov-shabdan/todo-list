import './Task.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

const Task = props => {
    return (
        <div className="task">
            <div>
                <div className="task-block">
                    <span className="task-title">{props.task}</span>
                    <span className="task-status"> TaskDone: {props.done.toString()}</span>
                    <div className="task-done">
                        <input type="checkbox" className="task-done-check" onChange={props.onCheck}/>
                    </div>
                    <button className="btn-remove" onClick={props.onRemove}>
                        <i className="far fa-trash-alt"></i>
                    </button>
                </div>

            </div>
        </div>
    );
}

export default Task;